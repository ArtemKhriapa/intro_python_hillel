print('\n')
print('hello world')


# comment in line 3
print('\n')
print('comment here -->')  # comment in here too


"""multiline comment
in here
and here """

print('\n')
for letter in 'hello':
    print(letter + '\n')  # indent is 4 spaces

# print('\n')
# import keyword
# print(keyword.kwlist)


# simplicity
print('\n')
print('example string')

print('\n')
my_str = 'some another string'
print(my_str)

print('1 + 2 is', 1+2)

print('\n')
# print != Print != PRINT
my_var = 10**20
MY_VAR = 10**200
print(my_var)
print(MY_VAR)
print(my_var is MY_VAR)

print('\n')

# dynamically typed language
print(type(my_str))
print(my_str)
my_str = 9**10
print(type(my_str))
print(my_str)

print('\n')

my_int = 100500
my_float = 100.5
my_str1 = "string variable"
my_str2 = 'string variable'

my_bool = True
my_bool2 = False
my_none = None


print('\n')

my_array1 = [1, 2, '3', True, None]
my_array2 = [my_int, my_str1, my_bool2, my_array1]

print(my_array2)





